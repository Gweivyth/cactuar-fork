--------------------------------------------------------
-- (C) 2010 Wings Project.                            --
-- Tonberry server customizations.                    --
-- Set HOOK_FILE_NAME = "tonberry" in settings.lua    --
-- to enable.                                         --
--------------------------------------------------------

function hookOnCharCreate(player)
    -- The CreateChar routine has already added a
    -- weapon according to the job selection but we
    -- need to add one more for dual-wielding
    -- All races get the starting ring
    local nation = player:getNation()
    local nationInfo = startingNationInfo[nation]
    if not player:hasItem(nationInfo.ring) then
        player:addItem(nationInfo.ring)
    end
    
    -- Echad ring
    if not player:hasItem(27556) then
        player:addItem(27556)
    end
end
